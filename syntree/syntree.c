#include "syntree.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int listInit(void **buffer, int *capacity, int *length, int item_size) {
	int start_size = 8;
	*buffer = malloc(start_size * item_size);

	if (*buffer == NULL) {
		fprintf(stderr, "Malloc ist in listInit fehlgeschlagen");
		return 21;
	}

	*capacity = start_size;
	*length = 0;

	return 0;
}

int listAppend(void **buffer, int *capacity, int *length, int itemsize, void *item) {
	if (*length == *capacity) {
		*capacity *= 2;
		*buffer = realloc(*buffer, *capacity * itemsize);

		if (*buffer == NULL) {
			fprintf(stderr, "Realloc ist in listAppend fehlgeschlagen");
			return 21;
		}
	}
	memcpy((char*)*buffer + *length * itemsize, item, itemsize);
	*length += 1;

	return 0;
}

int listPrepend(void **buffer, int *capacity, int *length, int itemsize, void *item) {
	if (*length == *capacity) {
		*capacity *= 2;
		*buffer = realloc(buffer, *capacity * itemsize);

		if (*buffer == NULL) {
			fprintf(stderr, "Realloc ist in listAppend fehlgeschlagen");
			return 21;
		}
	}
	memmove((char*)*buffer + itemsize, *buffer, *length * itemsize);
	memcpy(*buffer, item, itemsize);
	*length += 1;

	return 0;
}

int syntreeInit(Syntree *self) {
	return listInit((void**)&self->nodes, &self->capacity, &self->length, sizeof(SyntreeNode));
}

void syntreeRelease(Syntree *self) {
	for (int i = 0; i < self->length; i++) {
		SyntreeNode *node = &self->nodes[i];

		if (node->type == TYPE_LIST) {
			free(node->list.children);
		}
	}

	free(self->nodes);
}

SyntreeNodeID syntreeNodeNumber(Syntree *self, int number) {
	SyntreeNode node;
	node.type = TYPE_LEAF;
	node.leaf.value = number;

	listAppend((void**)&self->nodes, &self->capacity, &self->length, sizeof(SyntreeNode), &node);

	return self->length - 1;
}

SyntreeNodeID syntreeNodeList(Syntree *self) {
	SyntreeNode node;
	node.type = TYPE_LIST;

	listInit((void**)&node.list.children, &node.list.capacity, &node.list.length, sizeof(SyntreeNodeID));
	listAppend((void**)&self->nodes, &self->capacity, &self->length, sizeof(SyntreeNode), &node);

	return self->length - 1;
}

SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID id) {
	SyntreeNodeID list = syntreeNodeList(self);
	syntreeNodeAppend(self, list, id);

	return list;
}

SyntreeNodeID syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
	SyntreeNodeID list = syntreeNodeTag(self, id1);
	syntreeNodeAppend(self, list, id2);

	return list;
}

int syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem) {
	SyntreeNode *list_node = &self->nodes[list];

	if (list_node->type != TYPE_LIST) {
		fprintf(stderr, "syntreeNodeAppend called with a non-list node");
		exit(21);
	}

	listAppend((void**)&list_node->list.children, &list_node->list.capacity, &list_node->list.length, sizeof(SyntreeNodeID), &elem);

	return list;
}

SyntreeNodeID syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list) {
	SyntreeNode *list_node = &self->nodes[list];

	if (list_node->type != TYPE_LIST) {
		fprintf(stderr, "syntreeNodePrepend called with a non-list node");
		exit(21);
	}

	listPrepend((void**)&list_node->list.children, &list_node->list.capacity, &list_node->list.length, sizeof(SyntreeNodeID), &elem);

	return list;
}

void syntreePrintIndent(const Syntree *self, SyntreeNodeID root, int indentation) {
	SyntreeNode *node = &self->nodes[root];

	if (node->type == TYPE_LIST) {
		for (int i = 0; i < indentation; i ++) {
			printf("    ");
		}

		printf("{\n");
		
		for (int i = 0; i < node->list.length; i++) {
			syntreePrintIndent(self, node->list.children[i], indentation + 1);
		}
		
		for (int i = 0; i < indentation; i ++) {
			printf("    ");
		}

		printf("}\n");
	}
	else {
		for (int i = 0; i < indentation; i ++) {
			printf("    ");
		}
		
		printf("(%d)\n", node->leaf.value);
	}
}

void syntreePrint(const Syntree *self, SyntreeNodeID root) {
	syntreePrintIndent(self, root, 0);
}
