#include <stdio.h>
#include <stdlib.h>

struct Node
{
    int data;
    struct Node* next;
};

//创建Node
struct Node* createNode(int data){
    struct Node* newNode = (struct Node*) malloc(sizeof(struct Node));
    newNode ->data = data;
    newNode ->next = NULL;
    return newNode;
}

typedef struct stack{
    /* hier sollte noch etwas dazu kommen */
    struct Node* stackTop; //栈顶标记
    int size; //栈中元素个数
} IntStack;

//创建栈，就是创建一个struct stack的变量
struct stack* createStack(){
    //创建过程就是初始化过程
    struct stack* myStack = (struct stack*)malloc(sizeof(struct stack));//申请一块动态内存的标准格式
    myStack->stackTop = NULL;
    myStack->size = 0;
    return myStack;
}

int stackInit(IntStack *self){
    struct stack* myStack = (struct stack*)malloc(sizeof(struct stack));//申请一块动态内存的标准格式
    myStack->stackTop = NULL;
    myStack->size = 0;
    *self = *myStack;
    return 0;
}

//写函数的方式，
void stackPush(struct stack* myStack, int data){
    // 用data来创建一个Node
    struct Node* newNode = createNode(data);
    // 入栈操作就是
    newNode->next = myStack->stackTop;
    myStack->stackTop = newNode;
    myStack->size++;
}

//写函数的方式，
int stackPop(struct stack* myStack){
    //防御编程
    if (myStack->size == 0){
//        printf("栈顶为空，无法获取栈顶元素");
        fprintf(stderr, "%s", "Stack is empty!\n");
        exit(-1);
    }

    //暂存栈顶元素
    struct Node* freeNode = myStack->stackTop;
    int tempData = myStack->stackTop->data;
    myStack->stackTop = myStack->stackTop->next;
    free(freeNode);
    myStack->size--;
    return tempData;
}

int stackIsEmpty(struct stack* myStack){
    if(myStack->size == 0){
        return 1;
    } else return 0;
}

int stackTop(struct stack* myStack){
    //防御编程
    if (myStack->size == 0){
//        printf("栈顶为空，无法获取站定元素");
        fprintf(stderr, "%s", "Stack is empty!\n");
        exit(-1);
    }

    int temp = myStack->stackTop->data;
    return temp;
}

void stackRelease(struct stack* myStack){
    while(!stackIsEmpty(myStack)){
        stackPop(myStack);//\t是tab的意思
    }
}


//int main() {
//    IntStack stack;
//
//    if (stackInit(&stack) != 0){
//        return -1;
//    }
//
//    stackPush(&stack, 1);
//    stackPush(&stack, 2);
//    stackPush(&stack, 3);
//
//    while (!stackIsEmpty(&stack)){
//        printf("当前长度为：%d\n", stack.size);
//        printf("%i\n", stackPop(&stack));
//    }
//
//    stackPop(&stack);
//
//    stackRelease(&stack);
//    return 0;
//}
